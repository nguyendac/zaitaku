</article>

<footer>
	<div id="footer1">
		<div id="footer1_inner">
			<div class="footer1_1">
<h2>Contact<p></p>
<div class="jp">お問い合わせ</div>
</h2>
			</div>
			<div class="footer1_2">
				<li>
					
					<div class="text1">お電話でのお問い合せはこちら</div>
					<div class="text2"></div>
					<div class="text3">TEL.<a href="tel:03-6453-8936">03-6453-8936</a></div>
					<div class="text4">営業時間：000:00〜00:00</div>

				</li>
				<li class="end">
					<div class="text1">メールでのお問い合せはこちら</div>
					<div class="text5"><a href="#">CONTACT</a></div>
				</li>
			</div>
		</div>
	</div>
	<div id="footer2">
		<div id="footer2_inner">
		
			<div class="footer2_1">
				<h2><img src="<?php echo get_template_directory_uri(); ?>/img/footer_logo.png"></h2>
				<ul>
				<li>〒113-0033</li>
				<li>東京都文京区本郷1-7-10メディカル事業部: 東京都港区西新橋1-18-11 2F</li>
				</ul>
			</div>
			<div class="footer2_2">
			
				<ul>
				<li><a href="#">ホーム</a></li>
				<li><a href="#">企業概要</a></li>
				<li><a href="#">事業内容</a></li>
				<li><a href="#">お問い合わせ</a></li>
				</ul>
<!--
				<ul class="end">
				<li><a href="#">メニューが入ります</a></li>
				<li><a href="#">メニューが入ります</a></li>
				<li><a href="#">メニューが入ります</a></li>
				<li><a href="#">メニューが入ります</a></li>
				<li><a href="#">メニューが入ります</a></li>
				</ul>
-->
			</div>
			<div class="footer2_3">
				<p>Copyright &copy; ZAIKEN.co.ltd All rights reserved.</p>
			</div>
		</div>
	</div>
</footer>



<?php wp_footer(); ?>
</body>

</html>