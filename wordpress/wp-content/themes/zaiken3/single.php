<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<?php
	
		
// 	include $_SERVER[DOCUMENT_ROOT]."/wp-config.php";
	$db = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD);
	mysql_query("SET NAMES utf8");
	if(!mysql_select_db(DB_NAME, $db)){
// 		echo '<div style="color:red;">メインデータベースに接続できません。</div>';
	}elseif(mysql_select_db(DB_NAME, $db)){
// 		echo '<div style="color:green;">DB指定OK!('.DB_NAME.')</div>';
	}

	$sql = "SELECT * FROM `wp_options` WHERE `option_name` = 'home'";
	$rs = mysql_query($sql,$db);
	$item = mysql_fetch_assoc($rs);
	$url = $item[option_value];
	
	


	$id = get_the_ID();
// 	echo $id;
	
	$sql="SELECT * FROM `wp_posts` WHERE `ID` = '$id'";
	$rs = mysql_query($sql,$db);
	$item = mysql_fetch_assoc($rs);
// 	var_dump($item);

	$sql3 = "SELECT * FROM `wp_term_relationships` WHERE `object_id` = '$id'";
	$rs3 = mysql_query($sql3,$db);
	$item3 = mysql_fetch_assoc($rs3);
	
	$sql2 = "SELECT * FROM `wp_terms` WHERE `term_id` = '$item3[term_taxonomy_id]'";
	$rs2 = mysql_query($sql2,$db);
	$item2 = mysql_fetch_assoc($rs2);
	$catname = $item2[name];
	
	
	?>
	<?php
	echo '
	<div class="nav">
	<ol class="nav_inner" itemscope itemtype="http://schema.org/BreadcrumbList">
	<li class="bcl-first" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="'.$url.'"><span itemprop="name">MOTTO!!</span> TOP</a>　>　
	</li>';
	
	echo '
	<li class="bcl-second"  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a href="'.$url.'/category/'.$item2[slug].'"><span itemprop="name">'.$catname.'</span></a>
	</li>';
	
	if($id != ''){
		echo '
		<li class="bcl-last">　>　'.$item[post_title].'</li>
		';
	}
	
	?>
	
	</ol>
	</div>
	
	
	<div id="primary" class="content-area">
		<main id="main_single" class="site-main" role="main">


			<header class="entry-header">
				<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
					endif;
				?>
			</header><!-- .entry-header -->

			<?php
				// Post thumbnail.
				twentyfifteen_post_thumbnail();
			?>

			<div class="entry-content">
				<?php echo $post->post_content; ?>
				<div class="next_link">▶<a href="">続きを読む</a></div>
			</div><!-- .entry-content -->

			<?php
				// Author bio.
				if ( is_single() && get_the_author_meta( 'description' ) ) :
					get_template_part( 'author-bio' );
				endif;
			?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
