<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<?php
	
		
// 	include $_SERVER[DOCUMENT_ROOT]."/wp-config.php";
	$db = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD);
	mysql_query("SET NAMES utf8");
	if(!mysql_select_db(DB_NAME, $db)){
// 		echo '<div style="color:red;">メインデータベースに接続できません。</div>';
	}elseif(mysql_select_db(DB_NAME, $db)){
// 		echo '<div style="color:green;">DB指定OK!('.DB_NAME.')</div>';
	}

	$sql = "SELECT * FROM `wp_options` WHERE `option_name` = 'home'";
	$rs = mysql_query($sql,$db);
	$item = mysql_fetch_assoc($rs);
	$url = $item[option_value];
	
	


	$id = get_the_ID();
// 	echo $id;
	
	$sql="SELECT * FROM `wp_posts` WHERE `ID` = '$id'";
	$rs = mysql_query($sql,$db);
	$item = mysql_fetch_assoc($rs);
// 	var_dump($item);

	$sql3 = "SELECT * FROM `wp_term_relationships` WHERE `object_id` = '$id'";
	$rs3 = mysql_query($sql3,$db);
	$item3 = mysql_fetch_assoc($rs3);
	
	$sql2 = "SELECT * FROM `wp_terms` WHERE `term_id` = '$item3[term_taxonomy_id]'";
	$rs2 = mysql_query($sql2,$db);
	$item2 = mysql_fetch_assoc($rs2);
	$catname = $item2[name];
	
	
	?>
	<?php
	echo '
	<nav class="nav">
	<ol class="nav_inner" itemscope itemtype="http://schema.org/BreadcrumbList">
	<li class="bcl-first" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="'.$url.'"><span itemprop="name">MOTTO!!</span> TOP</a>　>　
	</li>';
	
	echo '
	<li class="bcl-second"  itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a href="'.$url.'/category/'.$item2[slug].'"><span itemprop="name">'.$catname.'</span></a>
	</li>';
	
	?>
	
	</ol>
	</nav>
	
	
	
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>


			<div style="clear: both;"></div>
				<?php
// 					the_archive_title( '<h1 class="page-title">「', '」の記事一覧</h1>' );
// 					the_archive_description( '<div class="taxonomy-description">', '</div>' );

					echo '<h1 class="cat-title">「';
					single_cat_title();
					echo '」の記事一覧</h1>';
				?>
			
			

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
