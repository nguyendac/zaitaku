<?php
require_once("meta-box-class/my-meta-box-class.php");
function enqueue_script(){
  wp_enqueue_script('libs',get_template_directory_uri().'/common/js/libs.js', '1.0', 1 );
  wp_enqueue_script('slick', get_template_directory_uri().'/common/js/slick/slick.js','1.0',1);
  if(is_front_page()) {
    wp_enqueue_script('toppage', get_template_directory_uri().'/js/script.js','1.0', 1 );
  }
  if(is_page()  || is_singular('infor')) {
    wp_enqueue_script('company', get_template_directory_uri().'/company/js/scripts.js','1.0', 1 );
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_script');
function enqueue_style() {
  if(is_front_page() || is_singular('infor')) {
    wp_enqueue_style('toppage',get_stylesheet_directory_uri().'/css/style.css');
  }
  if(is_page() ||is_singular('infor')) {
    wp_enqueue_style('company',get_stylesheet_directory_uri().'/company/css/style.css');
  }
}
add_action( 'wp_enqueue_scripts', 'enqueue_style' );
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');
function info_arr(){
  $args = array(
    'public'=>true,
    'labels' => array(
      'name'=> 'List Information',
      'singular_name'=> 'infor',
      'add_new_item'=> 'New Information'
    ),
    'supports' => array(
      'title',
      'excerpt',
      'editor'
    ),
  );
  register_post_type('infor', $args);
}
add_action('init','info_arr');
$config_info = array(
  'id' => 'intro',
  'title' => 'Intro',
  'pages' => array('infor'),
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(),
  'local_images' => true,
  'use_with_theme' => true
);
$info_meta = new AT_Meta_Box($config_info);
$info_meta->addImage('image_field', array('name' => 'Image Eyecatch','style'=>'max-width :100%; width:100%; height:auto'));
$info_meta->addRadio('pickup',array(1=>'None',2=>'pickup'),array('name'=> 'Pick Up', 'std'=> array(1)));
$info_meta->Finish();
function create_type_info(){
  $labels = array(
    'name'              => _x( 'Name Type Information', 'taxonomy general name' ),
    'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Type' ),
    'all_items'         => __( 'All Type' ),
    'parent_item'       => __( 'Parent Type' ),
    'parent_item_colon' => __( 'Parent Type:' ),
    'edit_item'         => __( 'Edit Type' ),
    'update_item'       => __( 'Update Type' ),
    'add_new_item'      => __( 'Add New Type' ),
    'new_item_name'     => __( 'New Type' ),
    'menu_name'         => __( 'Type' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'type_info' ),
  );
  register_taxonomy( 'type', array( 'infor' ), $args );
}
add_action( 'init', 'create_type_info', 0 );
function wpse_139269_term_radio_checklist($args) {
  if (!empty($args['taxonomy']) && (($args['taxonomy'] === 'type') )) {
    if (empty($args['walker']) || is_a($args['walker'], 'Walker')) { // Don't override 3rd party walkers.
      if (!class_exists('WPSE_139269_Walker_Category_Radio_Checklist')) {
        /**
         * Custom walker for switching checkbox inputs to radio.
         *
         * @see Walker_Category_Checklist
         */
        class WPSE_139269_Walker_Category_Radio_Checklist extends Walker_Category_Checklist {
          function walk($elements, $max_depth, $args = array()) {
            $output = parent::walk($elements, $max_depth, $args);
            $output = str_replace(
                    array('type="checkbox"', "type='checkbox'"), array('type="radio"', "type='radio'"), $output
            );
            return $output;
          }
        }
      }
      $args['walker'] = new WPSE_139269_Walker_Category_Radio_Checklist;
    }
  }
  return $args;
}
add_filter('wp_terms_checklist_args', 'wpse_139269_term_radio_checklist');
function action_6_info(){
  $args = array(
    'post_type' => 'infor',
    'orderby'   => 'id',
    'order'     => 'desc',
    'posts_per_page' => 6
  );
  $query = new WP_Query($args);
  return $query;
}
add_filter('get_6_info','action_6_info');
?>