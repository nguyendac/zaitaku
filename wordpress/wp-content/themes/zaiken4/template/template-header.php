<div class="hd_box">
  <h1 class="hd_box_logo"><a href="/"><img src="<?php bloginfo('template_url')?>/common/images/logo.png" alt=""></a></h1>
  <div id="humberger" class="humberger show_sp">
    <span></span>
    <span></span>
    <span></span>
  </div>
  <!--/.humberger-->
  <nav class="hd_box_nav" id="nav">
    <ul>
      <li><a href="/">ホーム<span class="jp">Home</span></a></li>
      <li><a href="/company">企業概要<span class="jp">Company</span></a></li>
      <li><a href="#">事業内容<span class="jp">Business</span></a></li>
      <li><a href="#">代表挨拶<span class="jp">Greeting</span></a></li>
      <li><a href="#">お問い合わせ<span class="jp">Contact</span></a></li>
    </ul>
  </nav>
  <!--./nav-->
</div>