<?php
/*
Template Name: HOME PAGE
*/
get_header();
?>
<body>
  <span class="active" id="loading"><i></i></span>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('template/template','header')?>
    </header>
    <main>
      <div class="banner">
        <picture>
          <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/banner_sp.jpg" />
          <img src="<?php bloginfo('template_url')?>/images/banner_pc.jpg?v=dd823ca334bbb95b72eb8a622a61b874" alt="test" />
        </picture>
        <p>
          医師だから、<span>できること</span><br>
          診てきたから、<span>わかること</span><br>
          医療をひとつに繋ぐ、<span><ins class="txt_g">Z</ins>AI<ins class="txt_g_02">K</ins>EN</span>
        </p>
      </div>
      <section class="st_service">
        <div class="row">
          <div class="st_service_t">
            <figure><img src="<?php bloginfo('template_url')?>/images/top02.png?v=3face7a516e5131fe1ab1f6effabc2fc" alt="Top service"></figure>
            <div class="st_service_t_l">
              <figure><img src="<?php bloginfo('template_url')?>/images/top05.png?v=9c95cd315149aaa100b915a2e836659e" alt="Left service"></figure>
              <h3>ZAIKENについて</h3>
              <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
            </div>
            <!--/.left-->
          </div>
          <!--/.st_service-->
          <div class="st_service_list">
            <h2 class="ttl_st">サービス</h2>
            <div class="st_service_list_gr">
              <article>
                <a href="#">
                  <div class="top_art">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/top08.png?v=ec50fcd927683c1a89ebff4c9d71e2c7" alt="Top 08">
                    </figure>
                    <h3>IoT</h3>
                    <em>デジタル医療時代に備えた対面医療と遠隔医療の共存スタイルをいち早く提案する</em>
                  </div>
                  <!--/.top_art-->
                  <div class="bottom_art">
                    <span>check</span>
                  </div>
                  <!--/.bottom_art-->
                </a>
              </article>
              <article>
                <a href="#">
                  <div class="top_art">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/top09.png?v=a74ae35fdf6a7a8a8a5f6d9273d9e055" alt="Top 09">
                    </figure>
                    <h3>医療再生</h3>
                    <em>医療再生の総称医療再生の総称医療再生の総称医療再生の総称医療再生の総称医療再生の総称</em>
                  </div>
                  <!--/.top_art-->
                  <div class="bottom_art">
                    <span>check</span>
                  </div>
                  <!--/.bottom_art-->
                </a>
              </article>
              <article>
                <a href="#">
                  <div class="top_art">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/top10.png?v=5c775dbc3e455a6270679a88f32cda14" alt="Top 10">
                    </figure>
                    <h3>研究</h3>
                    <em>研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称</em>
                  </div>
                  <!--/.top_art-->
                  <div class="bottom_art">
                    <span>check</span>
                  </div>
                  <!--/.bottom_art-->
                </a>
              </article>
            </div>
            <!--/.gr-->
          </div>
          <!--/.list-->
        </div>
      </section>
      <!--/.st_service-->
      <section class="st_concept">
        <h2 class="ttl_st">会社案内</h2>
        <div class="st_concept_l">
          <figure>
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_01.png" alt="sample 01">
              <figcaption>Concept<span>企業コンセプト</span></figcaption>
            </a>
          </figure>
          <figure>
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_02.png" alt="sample 02">
              <figcaption>Company<span>会社概要</span></figcaption>
            </a>
          </figure>
          <figure>
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_03.png" alt="sample 03">
              <figcaption>History<span>沿革</span></figcaption>
            </a>
          </figure>
          <figure>
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_04.png" alt="sample 04">
              <figcaption>Office<span>事業所案内</span></figcaption>
            </a>
          </figure>
        </div>
        <!--/.st_concept-->
      </section>
      <section class="st_information">
        <div class="row">
          <h2 class="ttl_st">INFOMATION<span>お知らせ</span></h2>
          <div class="st_information_l">
            <article>
              <a href="#">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/images/infor_01.jpg?v=fd2613fad5d84e0ec1c77ede94aa6da8" alt="Information 01">
                  <figcaption>お知らせ</figcaption>
                </figure>
                <div class="main_art">
                  <time datetime="2018-08-22">2018.08<ins>22</ins></time>
                  <em>タイトルが入ります</em>
                </div>
              </a>
            </article>
            <article>
              <a href="#">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/images/infor_02.jpg?v=0aa8599ae523897982f82b449cb61398" alt="Information 02">
                  <figcaption>PRESS</figcaption>
                </figure>
                <div class="main_art">
                  <time datetime="2018-08-17">2018.08<ins>17</ins></time>
                  <em>タイトルが入ります</em>
                </div>
              </a>
            </article>
            <article>
              <a href="#">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/images/infor_03.jpg?v=a09c6cd972252734f2e7b9a0347e2060" alt="Information 03">
                  <figcaption>CSR</figcaption>
                </figure>
                <div class="main_art">
                  <time datetime="2018-08-17">2018.08<ins>17</ins></time>
                  <em>タイトルが入ります</em>
                </div>
              </a>
            </article>
            <article>
              <a href="#">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/images/infor_04.jpg?v=aa3892ab7bd6569b48d0631d87bb8366" alt="Information 04">
                  <figcaption>お知らせ</figcaption>
                </figure>
                <div class="main_art">
                  <time datetime="2018-08-16">2018.08<ins>16</ins></time>
                  <em>タイトルが入ります</em>
                </div>
              </a>
            </article>
            <article>
              <a href="#">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/images/infor_05.jpg?v=e5ba1456bb3ff1b052ac8f2fa2c64ff4" alt="Information 05">
                  <figcaption>PRESS</figcaption>
                </figure>
                <div class="main_art">
                  <time datetime="2018-08-10">2018.08<ins>10</ins></time>
                  <em>タイトルが入ります</em>
                </div>
              </a>
            </article>
            <article>
              <a href="#">
                <figure>
                  <img src="<?php bloginfo('template_url')?>/images/infor_06.jpg?v=371e37df083765943bc9bb6726f8bb32" alt="Information 06">
                  <figcaption>CSR</figcaption>
                </figure>
                <div class="main_art">
                  <time datetime="2018-08-16">2018.08<ins>16</ins></time>
                  <em>タイトルが入ります</em>
                </div>
              </a>
            </article>
          </div>
          <!--/.list-->
          <a class="list_view" href="#">VIEW ALL</a>
        </div>
      </section>
      <!--/.st_information-->
      <section class="st_pickup">
        <h2 class="ttl_st">おすすめ情報</h2>
        <div class="st_pickup_l wrap">
          <div class="slider">
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample101.png?v=69f185823dc6ff2b34f75905e69da211" alt="sample 01">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample102.png?v=51680ca20951412842766a121ba41054" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample103.png?v=967558d59900cbda8c8715121601258b" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample104.png?v=f8699c42adcf448f6f73b601268d4f49" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample101.png?v=69f185823dc6ff2b34f75905e69da211" alt="sample 01">
              <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample102.png?v=51680ca20951412842766a121ba41054" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample103.png?v=967558d59900cbda8c8715121601258b" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample104.png?v=f8699c42adcf448f6f73b601268d4f49" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
          </div>
        </div>
        <!--/.st_pickup-->
      </section>
      <!--/.st_pickup-->
      <section class="st_contact">
        <div class="row">
          <h2 class="ttl_st">CONTACT<span>お問い合わせ</span></h2>
          <div class="st_contact_bx">
            <article>
              <h3>お電話でのお問い合わせはこちら</h3>
              <p>TEL.<a class="tel" href="tel:03-6453-8936">03-6453-8936</a></p>
              <em>営業時間：000:00〜00:00</em>
            </article>
            <article>
              <h3>メールでのお問い合わせはこちら</h3>
              <a class="link_contact" href="#">CONTACT</a>
            </article>
          </div>
          <!--/.box-->
        </div>
      </section>
      <!--/.st_contact-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('template/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
</body>
</html>