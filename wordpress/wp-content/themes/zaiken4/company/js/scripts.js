window.addEventListener('DOMContentLoaded',function(){
  $('.slider').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 6000,
    dots: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          arrows: false,
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 476,
        settings: {
          arrows: false,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });
  new Tab();
})

var Tab = (function(){
  function Tab(){
    // showArticle(0);
    var t = this;
    this._target = document.querySelectorAll('.tabs');
    Array.prototype.forEach.call(this._target,function(el,i){
      var _eles = el.querySelectorAll('a');
      Array.prototype.forEach.call(_eles,function(c){
        showArticle(c,0);
        c.addEventListener('click',function(e){
          e.preventDefault();
          this.parentNode.classList.add('active');
          Array.prototype.forEach.call(_eles,function(child){
            if(child!=c){
              child.parentNode.classList.remove('active');
            }
          });
          showArticle(this,this.dataset.tabs);
        })
      })
    })
  }
  function showArticle(el,tabNum) {
    arr = closest(el,'.bx_tabs').querySelector('.wrap_components').querySelectorAll('.ctn_tabs');
    Array.prototype.forEach.call(arr,function(a){
      if(a.dataset.tabs == tabNum){
        a.style.display = "block";
      }
      else {
        a.style.display = "none";
      }
    });
  }
  return Tab;
})()