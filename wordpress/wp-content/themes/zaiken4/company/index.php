<?php
/*
Template Name: COMPANY PAGE
*/
get_header();
?>
<body class="page page_ms">
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('template/template','header')?>
    </header>
    <main>
      <div class="gr_ttl">
        <h2 class="ttl">Company<span>会社概要</span></h2>
      </div>
      <!--/.gr_ttl-->
      <ul class="breadcrumb">
        <li><a href="/">HOME</a></li>
        <li>会社概要</li>
      </ul>
      <!--/.breadcrumb-->
      <div class="bx_tabs">
        <div class="row">
          <ul class="tabs" id="tabs">
            <li class="active"><a href="" data-tabs="0">Message<span>代表挨拶</span></a></li>
            <li><a href="" data-tabs="1">Company<span>会社概要</span></a></li>
            <li><a href="" data-tabs="2">Adviser<span>顧問</span></a></li>
          </ul>
          <div id="wrap_tabs_components" class="wrap_components">
            <div class="ctn_tabs" data-tabs="0">
              <section class="st_message">
                <h3 class="ttl_section">Message<span>代表挨拶</span></h3>
                <div class="bx_message">
                  <div class="bx_message_left">
                    <h4>見出し1見出し1見出し1見出し1見出し1見出し1見出し1見出し1見出し1見出し1</h4>
                    <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
                    <em><ins>代表取締役</ins><span>柳川 貴雄</span></em>
                  </div>
                  <!--/.left-->
                  <figure>
                    <img src="<?php bloginfo('template_url')?>/company/images/avt.jpg" alt="Avatar">
                  </figure>
                </div>
              </section>
              <!--/.st_message-->
            </div>
            <div class="ctn_tabs" data-tabs="1">
              <section class="st_company">
                <h3 class="ttl_section">Company<span>会社概要</span></h3>
                <div class="bx_company">
                  <dl>
                    <dt>社名</dt>
                    <dd>ダミーテキストダミーテキストダミーテキス</dd>
                  </dl>
                  <dl>
                    <dt>代表者</dt>
                    <dd>柳川　貴雄</dd>
                  </dl>
                  <dl>
                    <dt>所在地</dt>
                    <dd>
                      <em>〒113-0033　東京都文京区本郷1-7-10 <a class="txt_map" href="https://www.google.com/maps/@22.28882,114.192162,12z?hl=vi-VN">GoogleMap</a></em>
                      <em>メディカル事業部: 東京都港区西新橋1-18-11 2F <a class="txt_map" href="https://www.google.com/maps/@53.951175,14.175711,12z?hl=vi-VN">GoogleMap</a></em>
                    </dd>
                  </dl>
                  <dl>
                    <dt>設立</dt>
                    <dd>○○○○年</dd>
                  </dl>
                  <dl>
                    <dt>電話番号</dt>
                    <dd><a class="tel" href="tel:03-6453-8936">03-6453-8936</a></dd>
                  </dl>
                </div>
              </section>
              <!--/.st_company-->
            </div>
            <div class="ctn_tabs" data-tabs="2">
              <section class="st_staff">
                <h3 class="ttl_section">Adviser<span>顧問</span></h3>
                <div class="bx_staff">
                  <article>
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/company/images/img_staff.jpg" alt="Images staff">
                    </figure>
                    <div class="ctn_r">
                      <h4>小林 茂昭</h4>
                      <dl>
                        <dt>略歴</dt>
                        <dd>
                          <p>世界脳神経外科学会連盟名誉会長<br>
                          信州大学名誉教授<br>
                          社会医療法人財団　慈泉会相澤病院 理事　<br>
                          脳卒中・脳神経センター　名誉センター長 、医学研究センター名誉顧問、治験センター長<br>
                          1963年　信州大学医学部卒<br>
                          1971年　メイヨー医科大学院卒<br>
                          日本脳神経外科学会脳神経外科専門医・日本脳卒中学会脳卒中専門医<br>
                          米国脳神経外科学会脳神経外科専門医</p>
                        </dd>
                      </dl>
                      <dl>
                        <dt>説明</dt>
                        <dd>
                          <p>脳神経外科を専攻し、メイヨー医科大学院に留学。帰国後は信州大学医学部附属病院で杉田虔一郎に師事し、脳神経外科の手術・研究に従事。また、ロボット手術の機械を開発し、脳深部の腫瘍や動脈瘤の新たな手術方法を考案するなど脳外科分野の発展に大きく貢献し、米国脳神経外科学会の「国際功労賞」を日本人として初めて受賞。世界脳神経外科学会連盟名誉会長を務め、世界的な医療普及活動にも尽力。</p>
                        </dd>
                      </dl>
                      <dl>
                        <dt>主な受賞歴</dt>
                        <dd>
                          <p>米国脳神経外科学会国際功労賞 (日本人として初受賞)<br>
                          信毎賞<br>
                          齋藤眞賞国際賞<br>
                          メイヨー・クリニック学術業績功労賞</p>
                        </dd>
                      </dl>
                      <!--/.ctn_r-->
                    </article>
                    <article>
                      <figure>
                        <img src="<?php bloginfo('template_url')?>/company/images/img_staff.jpg" alt="Images staff">
                      </figure>
                      <div class="ctn_r">
                        <h4>苗字 名前</h4>
                        <dl>
                          <dt>略歴</dt>
                          <dd>
                            <p>世界脳神経外科学会連盟名誉会長<br>
                            信州大学名誉教授<br>
                            社会医療法人財団　慈泉会相澤病院 理事　<br>
                            脳卒中・脳神経センター　名誉センター長 、医学研究センター名誉顧問、治験センター長<br>
                            1963年　信州大学医学部卒<br>
                            1971年　メイヨー医科大学院卒<br>
                            日本脳神経外科学会脳神経外科専門医・日本脳卒中学会脳卒中専門医<br>
                            米国脳神経外科学会脳神経外科専門医</p>
                          </dd>
                        </dl>
                        <dl>
                          <dt>説明</dt>
                          <dd>
                            <p>脳神経外科を専攻し、メイヨー医科大学院に留学。帰国後は信州大学医学部附属病院で杉田虔一郎に師事し、脳神経外科の手術・研究に従事。また、ロボット手術の機械を開発し、脳深部の腫瘍や動脈瘤の新たな手術方法を考案するなど脳外科分野の発展に大きく貢献し、米国脳神経外科学会の「国際功労賞」を日本人として初めて受賞。世界脳神経外科学会連盟名誉会長を務め、世界的な医療普及活動にも尽力。</p>
                          </dd>
                        </dl>
                        <dl>
                          <dt>主な受賞歴</dt>
                          <dd>
                            <p>米国脳神経外科学会国際功労賞 (日本人として初受賞)<br>
                            信毎賞<br>
                            齋藤眞賞国際賞<br>
                            メイヨー・クリニック学術業績功労賞</p>
                          </dd>
                        </dl>
                      </div>
                      <!--/.ctn_r-->
                    </article>
                  </div>
                </div>
              </section>
              <!--/.st_staff-->
            </div>
          </div>
        </div>
      </div>
      <!--/.bx_tabs-->
      <section class="st_pickup">
        <h2 class="ttl_st">おすすめ情報</h2>
        <div class="st_pickup_l wrap">
          <div class="slider">
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample101.png" alt="sample 01">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample102.png" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample103.png" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample104.png" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample101.png" alt="sample 01">
              <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample102.png" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample103.png" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample104.png" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
          </div>
        </div>
        <!--/.st_pickup-->
      </section>
      <!--/.st_pickup-->
      <section class="st_contact">
        <div class="row">
          <h2 class="ttl_st">CONTACT<span>お問い合わせ</span></h2>
          <div class="st_contact_bx">
            <article>
              <h3>お電話でのお問い合わせはこちら</h3>
              <p>TEL.<a class="tel" href="tel:03-6453-8936">03-6453-8936</a></p>
              <em>営業時間：000:00〜00:00</em>
            </article>
            <article>
              <h3>メールでのお問い合わせはこちら</h3>
              <a class="link_contact" href="#">CONTACT</a>
            </article>
          </div>
          <!--/.box-->
        </div>
      </section>
      <!--/.st_contact-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('template/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
</body>

</html>