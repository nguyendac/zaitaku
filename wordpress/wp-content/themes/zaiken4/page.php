<?php
get_header();
?>
<body class="page page_ms">
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('template/template','header')?>
    </header>
    <main>
      <?php
        while ( have_posts() ) : the_post();
      ?>
      <div class="">
        <div class="gr_ttl">
          <h2 class="ttl"><?php the_field('ttl_en');?><span><?php the_title()?></span></h2>
        </div>
        <ul class="breadcrumb">
          <li><a href="/">HOME</a></li>
          <li>会社概要</li>
        </ul>
        <?php the_content();?>
        <section class="st_pickup">
          <h2 class="ttl_st">おすすめ情報</h2>
          <div class="st_pickup_l wrap">
          <div class="slider">
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample101.png" alt="sample 01">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample102.png" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample103.png" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample104.png" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample101.png" alt="sample 01">
              <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample102.png" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample103.png" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample104.png" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
          </div>
        </div>
          <!--/.st_pickup-->
        </section>
        <!--/.st_pickup-->
        <section class="st_contact">
          <div class="row">
            <h2 class="ttl_st">CONTACT<span>お問い合わせ</span></h2>
            <div class="st_contact_bx">
              <article>
                <h3>お電話でのお問い合わせはこちら</h3>
                <p>TEL.<a class="tel" href="tel:03-6453-8936">03-6453-8936</a></p>
                <em>営業時間：000:00〜00:00</em>
              </article>
              <article>
                <h3>メールでのお問い合わせはこちら</h3>
                <a class="link_contact" href="#">CONTACT</a>
              </article>
            </div>
            <!--/.box-->
          </div>
        </section>
        <!--/.st_contact-->
      </div>
      <?php endwhile; ?>
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('template/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
</body>
</html>