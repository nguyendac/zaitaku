<?php
/*
Template Name: HOME PAGE
*/
get_header();
?>
<body>
  <span class="active" id="loading"><i></i></span>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('template/template','header')?>
    </header>
    <main>
      <div class="banner">
        <picture>
          <source media="(max-width: 768px)" srcset="<?php bloginfo('template_url')?>/images/banner_sp.jpg" />
          <img src="<?php bloginfo('template_url')?>/images/banner_pc.jpg?v=dd823ca334bbb95b72eb8a622a61b874" alt="test" />
        </picture>
        <p id="fv_h2">
         <em class="effect"><strong class="txt_hira">医師だから、</strong><strong class="txt_mincho">できること</strong><br></em>
         <em class="effect"><strong class="txt_hira">診てきたから、</strong><strong class="txt_mincho">わかること</strong><br></em>
         <em class="effect"><strong class="txt_hira">医療をひとつに繋ぐ、</strong><strong class="txt_mincho"><ins class="txt_g">Z</ins>AI<ins class="txt_g_02">K</ins>EN</strong></em>
        </p>
      </div>
      <section class="st_service">
        <div class="row">
          <div class="st_service_t">
            <figure class="partial fadeIn"><img src="<?php bloginfo('template_url')?>/images/top02.png?v=3face7a516e5131fe1ab1f6effabc2fc" alt="Top service"></figure>
            <div class="st_service_t_l">
              <figure class="partial fadeIn"><img src="<?php bloginfo('template_url')?>/images/top05.png?v=9c95cd315149aaa100b915a2e836659e" alt="Left service"></figure>
              <h3 class="partial fadeIn">ZAIKENについて</h3>
              <p class="partial fadeIn">ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
            </div>
            <!--/.left-->
          </div>
          <!--/.st_service-->
          <div class="st_service_list">
            <h2 class="ttl_st partial fadeIn">サービス</h2>
            <div class="st_service_list_gr">
              <article class="slideLeft partial">
                <a href="#">
                  <div class="top_art">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/top08.png?v=ec50fcd927683c1a89ebff4c9d71e2c7" alt="Top 08">
                    </figure>
                    <h3>IoT</h3>
                    <em>デジタル医療時代に備えた対面医療と遠隔医療の共存スタイルをいち早く提案する</em>
                  </div>
                  <!--/.top_art-->
                  <div class="bottom_art">
                    <span>check</span>
                  </div>
                  <!--/.bottom_art-->
                </a>
              </article>
              <article class="slideLeft partial delay_03">
                <a href="#">
                  <div class="top_art">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/top09.png?v=a74ae35fdf6a7a8a8a5f6d9273d9e055" alt="Top 09">
                    </figure>
                    <h3>医療再生</h3>
                    <em>医療再生の総称医療再生の総称医療再生の総称医療再生の総称医療再生の総称医療再生の総称</em>
                  </div>
                  <!--/.top_art-->
                  <div class="bottom_art">
                    <span>check</span>
                  </div>
                  <!--/.bottom_art-->
                </a>
              </article>
              <article class="slideLeft partial delay_06">
                <a href="#">
                  <div class="top_art">
                    <figure>
                      <img src="<?php bloginfo('template_url')?>/images/top10.png?v=5c775dbc3e455a6270679a88f32cda14" alt="Top 10">
                    </figure>
                    <h3>研究</h3>
                    <em>研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称研究の総称</em>
                  </div>
                  <!--/.top_art-->
                  <div class="bottom_art">
                    <span>check</span>
                  </div>
                  <!--/.bottom_art-->
                </a>
              </article>
            </div>
            <!--/.gr-->
          </div>
          <!--/.list-->
        </div>
      </section>
      <!--/.st_service-->
      <section class="st_concept">
        <h2 class="ttl_st partial fadeIn">会社案内</h2>
        <div class="st_concept_l">
          <figure class="slideLeft partial">
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_01.png" alt="sample 01">
              <figcaption>Concept<span>企業コンセプト</span></figcaption>
            </a>
          </figure>
          <figure class="slideLeft partial delay_03">
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_02.png" alt="sample 02">
              <figcaption>Company<span>会社概要</span></figcaption>
            </a>
          </figure>
          <figure class="slideLeft partial delay_06">
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_03.png" alt="sample 03">
              <figcaption>History<span>沿革</span></figcaption>
            </a>
          </figure>
          <figure class="slideLeft partial delay_09">
            <a href="#">
              <img src="<?php bloginfo('template_url')?>/images/concept_04.png" alt="sample 04">
              <figcaption>Office<span>事業所案内</span></figcaption>
            </a>
          </figure>
        </div>
        <!--/.st_concept-->
      </section>
      <section class="st_information">
        <div class="row">
          <h2 class="ttl_st partial fadeIn">INFOMATION<span>お知らせ</span></h2>
          <div class="st_information_l">
            <ul>
            <?php
              $list = apply_filters('get_6_info','');
              while($list->have_posts()):$list->the_post();
              $type = wp_get_post_terms($post->ID,'type',array("fields" => "all"));
            ?>
            <li>
              <a href="<?php the_permalink()?>">
                <time datetime="<?php _e(get_the_date('Y-m-d'))?>"><?php _e(get_the_date('Y.m.d'));?></time>
              <?php
              if($type){
                echo '<span>'.$type[0]->name.'</span>';
              } else {
                echo '<span>'.'no cate'.'</span>';
              }
              ?>
              <em><?php the_title();?></em>
              </a>
            </li>
            <?php endwhile;wp_reset_query();?>
            </ul>
          </div>
          <!--/.list-->
          <a class="list_view partial fadeIn" href="#">VIEW ALL</a>
        </div>
      </section>
      <!--/.st_information-->
      <section class="st_pickup">
        <h2 class="ttl_st partial fadeIn">おすすめ情報</h2>
        <div class="st_pickup_l wrap">
          <div class="slider partial fadeIn">
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample101.png?v=69f185823dc6ff2b34f75905e69da211" alt="sample 01">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample102.png?v=51680ca20951412842766a121ba41054" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample103.png?v=967558d59900cbda8c8715121601258b" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample104.png?v=f8699c42adcf448f6f73b601268d4f49" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample101.png?v=69f185823dc6ff2b34f75905e69da211" alt="sample 01">
              <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample102.png?v=51680ca20951412842766a121ba41054" alt="sample 02">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/images/sample103.png?v=967558d59900cbda8c8715121601258b" alt="sample 03">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
            <figure>
              <a href="#">
                <img src="<?php bloginfo('template_url')?>/common/images/sample104.png?v=f8699c42adcf448f6f73b601268d4f49" alt="sample 04">
                <figcaption>PickUp<span>おすすめ情報</span></figcaption>
              </a>
            </figure>
          </div>
        </div>
        <!--/.st_pickup-->
      </section>
      <!--/.st_pickup-->
      <section class="st_contact">
        <div class="row">
          <h2 class="ttl_st partial fadeIn">CONTACT<span>お問い合わせ</span></h2>
          <div class="st_contact_bx">
            <article class="partial fadeIn">
              <h3>お電話でのお問い合わせはこちら</h3>
              <p>TEL.<a class="tel" href="tel:03-6453-8936">03-6453-8936</a></p>
              <em>営業時間：000:00〜00:00</em>
            </article>
            <article class="partial fadeIn">
              <h3>メールでのお問い合わせはこちら</h3>
              <a class="link_contact" href="#">CONTACT</a>
            </article>
          </div>
          <!--/.box-->
        </div>
      </section>
      <!--/.st_contact-->
    </main>
    <footer id="footer" class="footer">
      <?php get_template_part('template/template','footer')?>
    </footer>
  </div>
  <?php get_footer();?>
</body>
</html>