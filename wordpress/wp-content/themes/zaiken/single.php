<?php get_header(); ?>


<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

<div id="main_contents">
	
<div id="contents" class="post-p">

<h1 class="h1"><?php the_title(); ?></h1>
<!--本文-->
<?php the_content(); ?>
<!--/本文-->

<?php if(get_the_tags()){ ?>
<p class="tag">タグ：<?php the_tags('', ', '); ?></p>
<?php
}
?>

</div>

	
<div id="side">
 <?php get_sidebar(); ?>
</div>

</div>

<?php endwhile; ?>


<div class="posts-link">
<?php
$max_page = $wp_query->max_num_pages;
$now_page = get_query_var('paged');
if ($now_page == 0) {
  $now_page =1;
}
if ($max_page > $now_page) {
?>
<span class="nav-previous"><?php next_posts_link('以前の記事へ') ?></span>
<?php
}
if (is_paged()) {
?>
<span class="nav-next"><?php previous_posts_link('新しい記事へ') ?></span>
<?php
}
?>
</div>


	
	
</div>

<?php else : ?>

<h1 class="title">お探しの記事は見つかりませんでした。</h1>
<div class="contents">
<p>当サイト内を検索されますか？</p><br />
<?php get_search_form(); ?>
</div>
<?php endif; ?>

	

		  
</div>
<!--/main-contents-->


</div>
	
	
	
  </div>
	
	



<!--footer-->
<?php get_footer(); ?>
<!--/footer-->


</body>
</html>