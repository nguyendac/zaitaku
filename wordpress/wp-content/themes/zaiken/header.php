<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie lt-ie9 lt-ie8" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie lt-ie9" lang="ja"> <![endif]-->
<!--[if lte IE 9]> <html class="ie IE" lang="ja"> <![endif]-->
<head>
  <script src="https://use.typekit.net/npd1dbn.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png">
	
	<?php wp_head(); ?>

<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/script/respond.min.js"></script>
<![endif]-->
<!--[if lte IE 7]><script src="js/lte-ie7.js"></script><![endif]-->
</head>
<body <?php body_class(); ?>>
<header id="header">
	<div class="h_top">
		<div class="container">
			<div class="h_top_inner">
		    	<div class="h_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo.png" alt=""></a></div>
		    	<div class="h_contact sp_none">
		    		<div class="h_tel">
		    			<p class="h_head"><span>お気軽にお問い合せください</span></p>
		    			<p class="num"><strong><i class="fa fa-phone"></i>0790-42-0416</strong><span class="opentime">電話受付時間  8:30〜17:30</span></p>
		    		</div>
		    		<div class="h_aside">
		    			<div class="h_fontsize">
		    				<div id="fontSize">
		    					<p>文字サイズの変更</p>
		    					<p class="changeBtn">中</p>
		    					<p class="changeBtn">大</p>
		    				</div>
		    			</div>
		    			<div class="h_link">
			    			<ul>
							  <?php
							    wp_nav_menu( array(
							      'theme_location' => 'headerlink',
							      'container'      => '',
							      'menu_class'    =>''
							    ) );
							  ?>
			    			</ul>
			    		</div>
		    		</div>
		    		<div class="h_mail">
		    			<a href="http://nishioka1192.xsrv.jp/kasaicci/email_magazine/" class="btn__orange">
		    				<i><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-merumaga.png" alt="MAIL"></i>
		    				<strong>メールマガジン</strong>
		    			</a>
		    			<a href="http://nishioka1192.xsrv.jp/kasaicci/contact/" class="btn__blue">
		    				<i><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-mail.png" alt="MAIL"></i>
		    				<strong>お問い合せ</strong>
		    			</a>
		    		</div>
		    	</div>
		    </div>
	    </div>
    </div>
	<nav id="gNav">
		<div class="container">
	
			<ul class="nav">
				  <?php
				    wp_nav_menu( array(
				      'theme_location' => 'mainmenu',
				      'container'      => '',
				      'menu_class'    =>'menu'
				    ) );
				  ?>
				<li class="sp_menu">
	    			<div class="h_link">
		    			<a href="">サイトマップ</a>
		    			<a href="">アクセス</a>
		    		</div>
		    		<div class="h_mail">
		    			<a href="" class="btn__orange">
		    				<i><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-merumaga.png" alt="MAIL"></i>
		    				<strong>メールマガジン</strong>
		    			</a>
		    			<a href="" class="btn__blue">
		    				<i><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-mail.png" alt="MAIL"></i>
		    				<strong>お問い合せ</strong>
		    			</a>
		    		</div>
		    		<div class="h_tel">
		    			<p class="h_head"><span>お気軽にお問い合せください</span></p>
		    			<p class="num"><strong><i class="fa fa-phone"></i><a href="tel:0790-42-0416">0790-42-0416</a></strong><span class="opentime">電話受付時間 / 8:00〜17:30</span></p>
		    		</div>
				</li>
			</ul>
		</div>
		<div class="mask"></div>
	</nav>
</header>

<div id="side_btn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>email_magazine/"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/side-merumaga_sp.jpg" alt="" class="sp"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/side-merumaga_pc.jpg" alt="" class="sp_none"></a></div>

<?php if(is_front_page()): ?>
<div id="slide" class="changeArea">
	<?php
	$args = array(
	  'post_type' => 'slider',
	  'posts_per_page' => -1, /* 表示する数 */
	); ?>
	<?php $my_query = new WP_Query( $args ); ?>
	<ul class="slider">
		<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
		<!-- ▽ ループ開始 ▽ -->
		<li class="slide_li">
			<div class="slide_bg" style="background-image:url(<?php $cf_sample = SCF::get('slider_bg');$cf_sample = wp_get_attachment_image_src($cf_sample,'full');$imgUrl = esc_url($cf_sample[0]);echo $imgUrl;?>);background-repeat: no-repeat;background-position: center;background-size: cover;">
				<div class="container">
					<a href="<?php $slider_link = SCF::get('slider_link');echo $slider_link;?>">
					<?php
					$slider_img = SCF::get('slider_img');
					echo wp_get_attachment_image( $slider_img , 'full' );
					?>
					</a>
				</div>
			</div>
		</li>
		<!-- △ ループ終了 △ -->
		<?php endwhile; ?>
	</ul>
	<?php wp_reset_postdata(); ?>
</div>
<?php else: ?>
<?php endif; ?>
<main id="main" role="main" class="container changeArea">

	<div id="pankuzu">
		<div class="container">	
			<div class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
				<?php if(function_exists('bcn_display'))
				{
				bcn_display();
				}?>
			</div>
		</div>
	</div>