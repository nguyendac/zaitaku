<?php get_header(); ?>




<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<div id="main_contents">
<div id="contents" class="post-p">

	<h1 class="h1">
<?php the_title(); ?>
</h1>

<!--本文-->
<?php the_content(); ?>
<!--/本文-->
</div>
	
<div id="side">
 <?php get_sidebar(); ?>
</div>
</div>
	

<?php if(get_the_tags()){ ?>
<p class="tag">タグ：<?php the_tags('', ', '); ?></p>
<?php
}
?>


<?php endwhile; ?>


<?php else : ?>
<h1 class="border_t_ttl">お探しの記事は見つかりませんでした。</h1>
<div class="contents">
<p>当サイト内を検索されますか？</p><br />
<?php get_search_form(); ?>


<?php endif; ?>

	
	
	
</div>


<!--maincontents終了-->
</div>

<!--post終了-->
</div>






<!--footer-->
<?php get_footer(); ?>
<!--/footer-->


</body>
</html>