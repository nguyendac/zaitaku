<footer id="footer" class="changeArea">
	<div class="f_nav">
		<div class="container">
			<div class="f_nav__inner">
				<ul class="f_nav_box">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footermenu1',
							'container'      => '',
						) );
					?>
				</ul>
				<!-- <ul class="f_nav_box">
					<li>
						<a href="" class="f_nav__ttl">商工会議所</a>
						<ul>
							<li><a href="">会員/組織図</a></li>
							<li><a href="">入会案内</a></li>
							<li><a href="">所在地/マップ</a></li>
							<li><a href="">会館のご案内</a></li>
							<li><a href="">会館のご利用について</a></li>
						</ul>
					</li>
				</ul> -->
				<ul class="f_nav_box">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footermenu2',
							'container'      => '',
						) );
					?>
				</ul>
				<ul class="f_nav_box">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footermenu3',
							'container'      => '',
						) );
					?>
				</ul>
				<ul class="f_nav_box">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footermenu4',
							'container'      => '',
						) );
					?>
				</ul>
				<ul class="f_nav_box">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footermenu5',
							'container'      => '',
						) );
					?>
				</ul>
				<ul class="f_nav_box">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footermenu6',
							'container'      => '',
						) );
					?>
				</ul>

			</div>
		</div>
	</div>
	<div class="f_bottom">
		<div class="f_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo.png" alt=""></a></div>
		<p class="copy">Copyright &copy; <?php bloginfo( 'name' ); ?> All Rights Reserved.</p>
	</div>
</footer>
<p id="pageTop"><a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/pagetop.png" alt="pageTop"></a></p>



<?php wp_footer(); ?>
</body>

</html>
