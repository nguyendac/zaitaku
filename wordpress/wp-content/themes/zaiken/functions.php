<?php

// サイト共通のCSSの読み込み
    wp_enqueue_style( 'style', get_stylesheet_uri() );




//---------------------------------------------------------------------------
//  カスタムメニュー
//---------------------------------------------------------------------------
if ( ! function_exists( 'lab_setup' ) ) :
function lab_setup() {
  register_nav_menus( array(
    'mainmenu' => 'メインメニュー',
    'headerlink' => 'ヘッダーリンク',
    'footermenu1' => 'フッターメニュー1',
    'footermenu2' => 'フッターメニュー2',
    'footermenu3' => 'フッターメニュー3',
    'footermenu4' => 'フッターメニュー4',
    'footermenu5' => 'フッターメニュー5',
	'footermenu6' => 'フッターメニュー6'

  ) );
}
endif;
add_action( 'after_setup_theme', 'lab_setup' );

//---------------------------------------------------------------------------
//  カスタム投稿タイプ作成
//---------------------------------------------------------------------------
function create_post_type() {
  $sliderSupports = [
    'title',
    'thumbnail',
    'revisions'
  ];

  // add post type
  register_post_type( 'slider',
    array(
      'label' => 'スライダー登録',
      'public' => true,
      'has_archive' => false,
      'menu_position' => 5,
      'supports' => $sliderSupports,
      'rewrite' => array(
            'slug' => 'slider',
            'with_front' => false)
    )
  );
}
add_action( 'init', 'create_post_type' );


//---------------------------------------------------------------------------
//  テーマフォルダの画像を呼び出すときの画像パスを短くする
//---------------------------------------------------------------------------
function imagepassshort($arg) {
$content = str_replace('"img/', '"' . get_bloginfo('template_directory') . '/img/', $arg);
return $content;
}
add_action('the_content', 'imagepassshort');



//---------------------------------------------------------------------------
//  アイキャッチ画像を有効にする。
//---------------------------------------------------------------------------
add_theme_support('post-thumbnails');


//---------------------------------------------------------------------------
//  自動生成するpタグやbrタグを固定ページだけ取り除く
//---------------------------------------------------------------------------
remove_filter('the_content','wpautop');
add_filter('the_content','custom_content');
function custom_content($content){
if(get_post_type()=='page')
    return $content; //
else
 return wpautop($content);
}

//---------------------------------------------------------------------------
//  自動生成するpタグやbrタグをウィジェットから取り除く
//---------------------------------------------------------------------------
remove_filter ( 'widget_text_content', 'wpautop' );

//---------------------------------------------------------------------------
//  titleを自動生成する
//---------------------------------------------------------------------------
add_theme_support('title-tag');

//---------------------------------------------------------------------------
//  HTML5でのマークアップで出力する
//---------------------------------------------------------------------------
add_theme_support( 'html5',array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
) );


//---------------------------------------------------------------------------
//  ヘッダに勝手に挿入されるコードを消す
//---------------------------------------------------------------------------
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_generator' );
function remove_recent_comments_style() {
    global $wp_widget_factory;
    remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'remove_recent_comments_style' );



//---------------------------------------------------------------------------
//  絵文字対応スクリプトを削除する
//---------------------------------------------------------------------------
function disable_emoji() {
     remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
     remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
     remove_action( 'wp_print_styles', 'print_emoji_styles' );
     remove_action( 'admin_print_styles', 'print_emoji_styles' );
     remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
     remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
     remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );



//---------------------------------------------------------------------------
//  アップロードするＪＰＥＧファイルの画質を落とさない
//---------------------------------------------------------------------------
add_filter('jpeg_quality', function($arg){return 100;});


//---------------------------------------------------------------------------
//  メディアを記事に挿入する際、クラス名を書き込まない
//---------------------------------------------------------------------------
function remove_img_attr($html, $id, $alt, $title, $align, $size) {
    return preg_replace('/ class=[\'"]([^\'"]+)[\'"]/i', '', $html);
}
add_filter('get_image_tag','remove_img_attr', 10, 6);

//---------------------------------------------------------------------------
//  サイドバーウィジェット
//---------------------------------------------------------------------------
register_sidebar( array(
	'name' => __( 'Side Widget' ),
	'id' => 'side-widget',
	'before_widget' => '<div class="widget-container box">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="side_ttl">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
    'name' => __( 'アーカイブ用' ),
    'id' => 'side-list-widget',
    'before_widget' => '<div class="widget-container side-list-widget box widget_archive">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="side_ttl">',
    'after_title' => '</h3>'
) );

register_sidebar( array(
    'name' => __( 'Calendar Widget' ),
    'id' => 'side-calendar-widget',
    'before_widget' => '<div class="widget-container">',
    'after_widget' => '</div>',
    'before_title' => '<p class="schedule__ttl marker"><span>',
    'after_title' => '</span></p>',
) );

// register_sidebar( array(
// 	'name' => __( 'Footer Widget' ),
// 	'id' => 'footer-widget',
// 	'before_widget' => '<div class="widget-area"><ul><li class="widget-container">',
// 	'after_widget' => '</li></ul></div>',
// 	'before_title' => '<h3>',
// 	'after_title' => '</h3>',
// ) );


add_theme_support('menus');
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 298, 150 );



//---------------------------------------------------------------------------
//	抜粋
//---------------------------------------------------------------------------
add_post_type_support('page', 'excerpt');


//---------------------------------------------------------------------------
//	もっと見る
//---------------------------------------------------------------------------
function new_excerpt_more($more) {
	return '・・・';
}
add_filter('excerpt_more', 'new_excerpt_more');

//---------------------------------------------------------------------------
//	ニューズ記事一覧
//---------------------------------------------------------------------------

//ここから
function Include_my_php($params = array()) {
    extract(shortcode_atts(array(
        'file' => 'default'
    ), $params));
    ob_start();
    include(get_theme_root() . '/' . get_template() . "/$file.php");
    return ob_get_clean();
}

add_shortcode('myphp', 'Include_my_php');
//ここまで




add_action( 'save_post', 'save_default_thumbnail' );
function save_default_thumbnail( $post_id ) {
	$post_thumbnail = get_post_meta( $post_id, $key = '_thumbnail_id', $single = true );
	if ( !wp_is_post_revision( $post_id ) ) {
		if ( empty( $post_thumbnail ) ) {
			update_post_meta( $post_id, $meta_key = '_thumbnail_id', $meta_value = '5' );
		}
	}
}
