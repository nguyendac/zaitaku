<?php get_header(); ?>



          <div class="js-tab_content news active">
            <div class="tab_inner">
              <?php
              $args = array(
              'posts_per_page' => 5 ,// 表示件数の指定
              'category_name' => 'news'
              );
              $posts = get_posts( $args );
              foreach ( $posts as $post ): // ループの開始
              setup_postdata( $post ); // 記事データの取得
              ?>
              <?php
               $cat = get_the_category();
               $cat = $cat[0];
              ?>
              <a href="<?php the_permalink(); ?>">
                <p class="date"><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time( 'Y年m月d日' ); ?></time></p>
                <p class="cat cat-<?php echo $cat->category_nicename; ?>">
                  <?php $cat = get_the_category(); ?>
                  <?php $cat = $cat[0]; ?>
                  <?php echo get_cat_name($cat->term_id); ?>
                </p>
                <p class="news_ttl"><?php the_title(); ?></p>
              </a>
              <?php
              endforeach; // ループの終了
              wp_reset_postdata(); // 直前のクエリを復元する
              ?>
            </div>
          </div><!-- /.js-tab_content -->

          <div class="js-tab_content exam">
            <div class="tab_inner">
              <?php
              $args = array(
              'posts_per_page' => 5 ,// 表示件数の指定
              'category_name' => 'exam'
              );
              $posts = get_posts( $args );
              foreach ( $posts as $post ): // ループの開始
              setup_postdata( $post ); // 記事データの取得
              ?>
              <?php
               $cat = get_the_category();
               $cat = $cat[0];
              ?>
              <a href="<?php the_permalink(); ?>">
                <p class="date"><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time( 'Y年m月d日' ); ?></time></p>
                <p class="cat cat-<?php echo $cat->category_nicename; ?>">
                  <?php $cat = get_the_category(); ?>
                  <?php $cat = $cat[0]; ?>
                  <?php echo get_cat_name($cat->term_id); ?>
                </p>
                <p class="news_ttl"><?php the_title(); ?></p>
              </a>
              <?php
              endforeach; // ループの終了
              wp_reset_postdata(); // 直前のクエリを復元する
              ?>
            </div>
          </div><!-- /.js-tab_content -->

          <div class="js-tab_content quality">
            <div class="tab_inner">
              <?php
              $args = array(
              'posts_per_page' => 5 ,// 表示件数の指定
              'category_name' => 'quality'
              );
              $posts = get_posts( $args );
              foreach ( $posts as $post ): // ループの開始
              setup_postdata( $post ); // 記事データの取得
              ?>
              <?php
               $cat = get_the_category();
               $cat = $cat[0];
              ?>
              <a href="<?php the_permalink(); ?>">
                <p class="date"><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time( 'Y年m月d日' ); ?></time></p>
                <p class="cat cat-<?php echo $cat->category_nicename; ?>">
                  <?php $cat = get_the_category(); ?>
                  <?php $cat = $cat[0]; ?>
                  <?php echo get_cat_name($cat->term_id); ?>
                </p>
                <p class="news_ttl"><?php the_title(); ?></p>
              </a>
              <?php
              endforeach; // ループの終了
              wp_reset_postdata(); // 直前のクエリを復元する
              ?>
            </div>
          </div><!-- /.js-tab_content -->
        </div>

        <a href="http://nishioka1192.xsrv.jp/kasaicci/category/news/" class="btn">新着情報一覧を見る</a>
      </section>

  <div id="side">
    <?php get_sidebar(); ?>
  </div>
<!--end-->
</div>

<!--footer-->
<?php get_footer(); ?>
<!--/footer -->
