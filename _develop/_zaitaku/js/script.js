window.addEventListener('DOMContentLoaded',function(){
  $('.slider').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 6000,
    dots: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          arrows: false,
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 476,
        settings: {
          arrows: false,
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  })
  $('body').css({
    overflow: 'hidden',
  })
  setTimeout(function(){
    $('#loading').addClass('end');
    $('body').css({
      overflow: 'inherit',
    })
  },3000);
})