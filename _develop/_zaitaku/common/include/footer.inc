<div class="bx_ft">
  <div class="row">
    <div class="bx_ft_l">
      <h2><a href="/"><img src="/common/images/logo.png" alt=""></a></h2>
      <ul>
        <li>〒113-0033</li>
        <li>東京都文京区本郷1-7-10メディカル事業部: 東京都港区西新橋1-18-11 2F</li>
      </ul>
    </div>
    <!--/.left-->
    <div class="bx_ft_r">
      <ul>
        <li><a href="#">ホーム</a></li>
        <li><a href="#">企業概要</a></li>
        <li><a href="#">事業内容</a></li>
        <li><a href="#">お問い合わせ</a></li>
      </ul>
    </div>
    <!--/.right-->
  </div>
  <div class="copyright">
    <div class="row">
      <p>Copyright &copy; ZAIKEN.co.ltd All rights reserved.</p>
    </div>
  </div>
</div>